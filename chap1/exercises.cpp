#include"exercises.h"
#include<iostream>
#include<vector>
#include<math.h>
#include<iterator>

using namespace std;

void Exercises::exNumber(int p_number)
{
    cout<<"************************"<<endl;
    cout<<"*         Zad "<<p_number<<"        *"<<endl;
    cout<<"************************"<<endl<<endl;
}

void Exercises::ex1()
{

    exNumber(1);

    cout<<"Podaj gorne ograniczenie"<<endl;
    unsigned int upLim;
    cin>>upLim;

    long int sum = 0;

    for(int  i = 0;i<=upLim;++i)
    {
        if(i % 3 ==0 && i % 5 == 0)
        sum = sum + i;
    }

    cout<<" Suma liczb podzielnych przez 3 i 5 wynosi: "<<sum<<endl;
}

void Exercises::ex2()
{
    exNumber(2);

    cout<<"Podaj liczbe calkowita"<<endl;
    int a,b;
    cin>>a;
    cout<<"Podaj liczbe calkowita"<<endl;
    cin>>b;

    auto gcd = [&a,&b]()
    {
        int tmp;
        while (b != 0) 
        {
            tmp = a;
            a = b;
            b = tmp % b;
        }   
        return a;
    };

    cout<<"NWD = "<<gcd()<<endl;
}

void Exercises::ex3()
{
    exNumber(3);

    cout<<"Podaj liczbe calkowita"<<endl;
    int a,b,a1,b1;
    cin>>a;
    cout<<"Podaj liczbe calkowita"<<endl;
    cin>>b;
    a1= a;
    b1 = b;

    auto gcd = [&a,&b]()
    {
        int tmp;
        while (b != 0) 
        {
            tmp = a;
            a = b;
            b = tmp % b;
        }   
        return a;
    };

     cout<<"NWD = "<<gcd()<<endl;

    auto lcm = [&a1,&b1,&gcd]()
    {
        int h = gcd();
        return h ? (a1*(b1/h)) : 0;
    };

    cout<<"NWW = "<<lcm()<<endl;
}

void Exercises::ex4()
{
    exNumber(4);

    cout<<"Podaj gorne ograniczenie"<<endl;
    int a;
    cin>>a;

    auto isPrime = [](int a )
    {
        if(a<=3)
        {
            return a>1;
        }
        else if(a % 2 == 0 || a % 3 == 0)
        {
            return false;
        }
        else
        {
            for (int i = 5; i*i <= a; i+=6)
            {
                if(a % i == 0 || a % (i+2) == 0)
                return false;
            }
                
            return true;
        }
        
    };

    for(int i = a; i>1; i--)
    {
        if(isPrime(i))
        {
            cout<<"Najmniejsza liczba pierwsza mniejsza od podanej to "<<i<<endl;
            break;
        }
    }
}

void Exercises::ex5()
{
    exNumber(5);

    auto isPrime = [](int a )
    {
        if(a<=3)
        {
            return a>1;
        }
        else if(a % 2 == 0 || a % 3 == 0)
        {
            return false;
        }
        else
        {
            for (int i = 5; i*i <= a; i+=6)
            {
                if(a % i == 0 || a % (i+2) == 0)
                return false;
            }
                
            return true;
        }
        
    };

    cout<<"Podaj gorne ograniczenie"<<endl;
    int a;
    cin>>a;

    for(auto i =2;i<=a;++i)
    {
        if(isPrime(i) && isPrime(i+6))
        cout<<i<<" : "<<i+6<<endl;
    }
}

void Exercises::ex9()
{
    exNumber(9);

    cout<<"Podaj liczbe calkowita"<<endl;
    int a;
    cin>>a;

    auto primeFactors = [&a]()
    {
        vector<unsigned long long> factors;
        while(a % 2 == 0)
        {
            a = a / 2;
            factors.push_back(2);
        }
        for(unsigned long long i =3; i<= sqrt(a); i+= 2)
        {
            while( a % i == 0)
            {
                a = a / i;
                factors.push_back(i);
            }
        }

        if(a > 2)
        factors.push_back(a);
        
        return factors;
    };

    auto factors = primeFactors();

    copy(begin(factors),end(factors),ostream_iterator<unsigned long long>(cout," "));
}
