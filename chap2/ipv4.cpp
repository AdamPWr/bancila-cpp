#include"ipv4.h"
#include<sstream>

ipv4& ipv4::operator=(const ipv4 & obj)
{
    data = obj.data;
    return *this;
}

string ipv4::to_string()
{
    stringstream sstr;

    sstr << *this;
    return sstr.str();
}

ostream& operator<<(ostream& os, ipv4& address)
{
    os<<static_cast<int>(address.data[0])<<".";
    os<<static_cast<int>(address.data[1])<<".";
    os<<static_cast<int>(address.data[2])<<".";
    os<<static_cast<int>(address.data[3]);

    return os;
}

istream& operator>>(istream& is, ipv4& address)
{
    char d1,d2,d3;
    int b1,b2,b3,b4;

    is>>b1>>d1>>b2>>d2>>b3>>d3>>b4;
    if(d1 =='.' && d2 =='.' && d3 =='.')
    {
        address = ipv4(b1,b2,b3,b4);
    }
    else
    {
        is.setstate(ios_base::failbit);
    }
    return is;
}

unsigned long ipv4::to_long()
{
    return (static_cast<unsigned long>(data[0])<<24 |
            static_cast<unsigned long>(data[1])<<16 |
            static_cast<unsigned long>(data[2])<<8 |
            static_cast<unsigned long>(data[3]) );
}