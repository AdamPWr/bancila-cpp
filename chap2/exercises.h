#pragma once

#include<iostream>
#include"ipv4.h"


class Exercises
{


    void showExTitle(short p_number);

    template<typename T>
    T minimum(const T a, const T b);

    template<typename T1,typename... T>
    T1 minimum(T1 a, T... args);

    template<typename C, typename... Args>
    void push_back(C& container,Args&&... args);

public:

    void ex1();
    void ex2();
    void ex3();
};

template<typename T>
T Exercises::minimum(const T a, const T b)
{
    return a>b ? b:a;
}

template<typename T1,typename... T>
T1 Exercises::minimum(T1 a, T... args)
{
    return minimum(a,minimum(args...));
}

template<typename C, typename... Args>
void Exercises::push_back(C& container,Args&&... args)
{
    (container.push_back(args), ...);
}