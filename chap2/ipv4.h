#pragma once
#include<array>
#include<string>

using namespace std;

class ipv4
{
    std::array<unsigned char,4> data;
public:

    constexpr ipv4() : data{{0}} {}
    constexpr ipv4(unsigned char a, unsigned char b,unsigned char c,unsigned char d):
    data{{a,b,c,d}} {}
    explicit constexpr ipv4(unsigned long address):
    data
    {
        static_cast<unsigned char> ((address>>24) & 0xFF),
        static_cast<unsigned char> ((address>>16) & 0xFF),
        static_cast<unsigned char> ((address>>8) & 0xFF),
        static_cast<unsigned char> (address & 0xFF)
    } {}

    ipv4(const ipv4 & obj): data(obj.data){}
    ipv4& operator=(const ipv4 & obj);
    string to_string();
    unsigned long to_long();
    friend ostream& operator<<(ostream& os, ipv4& address);
    friend istream& operator>>(istream& is, ipv4& address);
    
};